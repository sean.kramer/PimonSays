package net.seantech;

import java.util.Scanner;

public class CMD {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Driver driver = new Driver();
		Scanner in = new Scanner(System.in);
		System.out.println(Driver.INITIAL_STATE);
		while (true) {
			int i = in.nextInt();
			for (State s : driver.input(i % 3 != 1)) {
				System.out.println(s);
			}
		}
	}
	
}
