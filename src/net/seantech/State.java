package net.seantech;

import java.util.Arrays;

public class State {
	
	public static final String[] MODE_NAMES = {"NONE", "LOSS", "SHOW", "INPUT"};
	
	public static final int NONE = -1;

	public static final int LOSS = 0;
	public static final int SHOW = 1;
	public static final int INPUT = 2; 

	public final int pos, state;

	public State(int pos, int state) {
		this.pos = pos;
		this.state = state;
	}
	
	@Override
	public String toString() {
		int[] out = new int[4];
		if (pos >= 0 && pos < 4 && state != LOSS) {
			out[pos] = 1;
		} else if (pos >= 0) {
			System.out.println("Final Score: " + pos);
		}
		return Arrays.toString(out) + " " + MODE_NAMES[state + 1];
	}
	
}
