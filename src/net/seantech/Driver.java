package net.seantech;

import java.util.ArrayList;
import java.util.List;

public class Driver {

	public static final State INITIAL_STATE = new State(State.NONE, State.LOSS);
	public static final int LOOP_SIZE = 4;
	public static final int MODES = Mode.values().length;
	
	private int state = State.LOSS;
	private int mode = 0;
	private int moveCount = 0;
	
	private PimonSays game;
	
	public synchronized List<State> input(boolean move) {
		List<State> result = new ArrayList<State>();
		
		// Handle game mode picking
		if (state == State.LOSS) {
			// Start game after picking game mode
			if (move) {
				game = new PimonSays(LOOP_SIZE, Mode.values()[mode]);
			} else {
				mode = (mode + 1) % MODES;
				result.add(new State(State.NONE, mode));
				return result;
			}
			
		// Handle input while reading in sequence
		} else if (state == State.INPUT) {
			// Check for loss
			int pos = game.input(move);
			if (pos < 0) {
				result.add(new State(game.getSize() - 1, State.LOSS));
				state = State.LOSS;
				return result;
			} else {
				result.add(new State(pos, State.INPUT));
			}
			
			// Decrement remaining moves in sequence and check if finished
			moveCount--;
			if (moveCount == 0) {
				// Switch back to demo mode
				state = State.SHOW;	
			} else {
				return result;
			}
		}
			
		// If we've fallen through to here, show the next step in the sequence
		result.add(new State(game.START_POS, State.SHOW));
		for (int pos : game) {
			result.add(new State(pos, State.SHOW));
		}
		
		// Signal end of sequence demo
		result.add(new State(game.START_POS, State.INPUT));
			
		// Switch over to accepting input
		moveCount = game.getSize();
		state = State.INPUT;
		
		return result;
	}
	
	public int getState() {
		return state;
	}
	
}
