package net.seantech;

import java.util.List;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

public class GPIO {

	private final GpioController io = GpioFactory.getInstance();
	
	private GpioPinDigitalOutput[] multi = new GpioPinDigitalOutput[3];
	private GpioPinDigitalOutput[] array = new GpioPinDigitalOutput[4];
	
	private State current = Driver.INITIAL_STATE;
	private Driver driver = new Driver();
	
	
	public GPIO() {
		// Set up input switches
		io.provisionDigitalInputPin(RaspiPin.GPIO_03, PinPullResistance.PULL_DOWN)
			.addListener((GpioPinListenerDigital) (event -> input(event, false)));
		io.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_DOWN)
			.addListener((GpioPinListenerDigital) (event -> input(event, true)));
		
		System.out.println("PINS REGISTERED");
		
		multi[0] = io.provisionDigitalOutputPin(RaspiPin.GPIO_24);
		multi[1] = io.provisionDigitalOutputPin(RaspiPin.GPIO_28);
		multi[2] = io.provisionDigitalOutputPin(RaspiPin.GPIO_29);
		
		array[0] = io.provisionDigitalOutputPin(RaspiPin.GPIO_21);
		array[1] = io.provisionDigitalOutputPin(RaspiPin.GPIO_22);
		array[2] = io.provisionDigitalOutputPin(RaspiPin.GPIO_11);
		array[3] = io.provisionDigitalOutputPin(RaspiPin.GPIO_10);
		
		applyState(current);
	}
	
	
	private void input(GpioPinDigitalStateChangeEvent event, boolean move) {
		System.out.println(move);
		
		// Ignore release
		if (event.getState() == PinState.LOW) { return; }

		// Ignore during demo
		if (driver.getState() != State.LOSS && current.state == State.SHOW) { return; }
		
		List<State> states = driver.input(move);
		for (State state : states) {
			// Transition from demo to input
			if (current.state == State.SHOW && state.state == State.INPUT) {
				applyState(new State(State.NONE, State.INPUT));
				sleep(500);
				applyState(new State(State.NONE, State.NONE));
				sleep(500);
				applyState(new State(State.NONE, State.INPUT));
				sleep(500);
			} else if (current.state == State.INPUT && state.state == State.SHOW) {
				int old = current.pos;
				current = state; // Prevent threading issues
				applyState(new State(old, State.SHOW));
				sleep(500);
				applyState(new State(State.NONE, State.NONE));
				sleep(500);
				applyState(new State(State.NONE, State.SHOW));
				sleep(500);
			}
			current = state;
			applyState(current);
			if (current.state == State.SHOW) { sleep(600); }
		}
	}
	
	
	private void applyState(State state) {
		System.out.println(state + " " + State.MODE_NAMES[driver.getState()]);
		for (int i = 0; i < array.length; i++) {
			if (i == state.pos && state.state != State.LOSS) { array[i].high(); }
			else { array[i].low(); }
		}
		for (int i = 0; i < multi.length; i++) {
			if (i == state.state) { multi[i].high(); }
			else { multi[i].low(); }
		}
		
		// Blink logic
	}
	
	
	private void sleep(long ms) {
		if (driver.getState() == State.LOSS) { return; }
		try { Thread.sleep(ms); }
		catch (Exception e) {}
	}
	
	
	public static void main(String[] args) throws Exception {
		new GPIO();
		while (true) {
			Thread.sleep(500);
		}
	}
	
	
	
	
}
