package net.seantech;

import java.util.ArrayList;
import java.util.Iterator;

public class PimonSays implements Iterable<Integer>, Iterator<Integer> {
	
	public final int LOOP_SIZE;
	public final Mode MODE;
	public final int START_POS;
	
	private ArrayList<Boolean> pattern = new ArrayList<>();
	private int iterator, ipos;
	private int input_index, input_pos;
	
	
	public PimonSays(int loopSize, Mode gameMode) {
		LOOP_SIZE = loopSize;
		MODE = gameMode;
		START_POS = (int) (Math.random() * LOOP_SIZE);
	}
	
	
	public void add() {
		pattern.add(Math.random() >= 0.5);
	}
	
	
	public int input(boolean move) {
		// Wrong value
		if (move != pattern.get(input_index++)) { return -1; }
		// Return next light position
		input_pos = adjust(input_pos, move);
		return input_pos;
	}
	
	
	public int getSize() {
		return pattern.size();
	}
	
	
	private int adjust(int current, boolean move) {
		int pos = (current + MODE.adjust(move)) % LOOP_SIZE;
		if (pos < 0) { pos += LOOP_SIZE; }
		return pos;
	}
	
	
	@Override
	public boolean hasNext() {
		return iterator < pattern.size();
	}

	
	@Override
	public Integer next() {
		ipos = adjust(ipos, pattern.get(iterator++));
		return ipos;	
	}

	
	@Override
	public Iterator<Integer> iterator() {
		iterator = 0;
		ipos = START_POS;
		input_index = 0;
		input_pos = START_POS;
		add();
		return this;
	}
	

}
