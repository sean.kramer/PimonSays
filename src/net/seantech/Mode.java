package net.seantech;

public enum Mode {
	
	LEFT_RIGHT(-1, 1),
	REPEAT_FORWARD(0, 1),
	SELECT_ENTER(1, 0);
	
	private final int falseCondition, trueCondition;
	
	Mode(int falseCondition, int trueCondition) {
		this.falseCondition = falseCondition;
		this.trueCondition = trueCondition;
	}
	
	public int adjust(boolean move) {
		return move ? trueCondition : falseCondition;
	}

}
